/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zetahealth.phms.data.manager;

import com.zetahealth.phms.data.provider.DataProviderLocal;
import com.zetahealth.phms.model.Pharmacy;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */
@Stateless
public class PharmacyDataManager implements PharmacyDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public Pharmacy create(Pharmacy pharmacy) throws EJBTransactionRolledbackException {
        return crud.create(pharmacy);
    }

    @Override
    public Pharmacy get(String pcnId) {
        return crud.find(pcnId, Pharmacy.class);
    }

}

