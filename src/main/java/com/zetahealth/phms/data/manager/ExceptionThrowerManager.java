/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zetahealth.phms.data.manager;

import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import com.zetahealth.phms.util.exception.GeneralAppException;

/**
 *
 * @author buls
 */

@Stateless
public class ExceptionThrowerManager implements ExceptionThrowerManagerLocal {
   
    private final String INCOMPLETE_DATA_ERROR = "Incomplete data";
    private final String INCOMPLETE_DATA_ERROR_DETAILS = "Incomplete data has been provided";
    
    private final String INVALID_TOKEN_ERROR = "Invalid or expired token supplied";
    private final String INVALID_TOKEN_ERROR_DETAILS = "The auth token supplied is invalid or expired";
    
    public final String PHARMACY_ALREADY_EXISTS_ERROR = "Pharmacy already exists";
    public final String PHARMACY_ALREADY_EXISTS_ERROR_DETAILS = "A pharmacy already exists with the pcn id supplied";
    
    
    @Override
    public void throwNullPharmacyAttributesException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                    400, INCOMPLETE_DATA_ERROR, INCOMPLETE_DATA_ERROR_DETAILS, link);
    }
    
    @Override
    public void throwInvalidTokenException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_TOKEN_ERROR, INVALID_TOKEN_ERROR_DETAILS, link);
    }
    
    @Override
    public void throwPharmacyAlreadyExistException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                    400, PHARMACY_ALREADY_EXISTS_ERROR, PHARMACY_ALREADY_EXISTS_ERROR_DETAILS, link);
    }
    
}
