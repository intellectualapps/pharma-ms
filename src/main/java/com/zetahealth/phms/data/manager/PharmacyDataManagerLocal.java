/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zetahealth.phms.data.manager;

import com.zetahealth.phms.model.Pharmacy;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;

/**
 *
 * @author Lateefah
 */
@Local
public interface PharmacyDataManagerLocal {
    
    Pharmacy create(Pharmacy pharmacy) throws EJBTransactionRolledbackException;

    Pharmacy get(String pcnId);

}
