/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zetahealth.phms.data.manager;

import com.zetahealth.phms.util.exception.GeneralAppException;
/**
 *
 * @author Lateefah
 */
public interface ExceptionThrowerManagerLocal {
   
    void throwNullPharmacyAttributesException(String link) throws GeneralAppException;
    
    void throwInvalidTokenException(String link) throws GeneralAppException;
            
    void throwPharmacyAlreadyExistException(String link) throws GeneralAppException;
    
}
