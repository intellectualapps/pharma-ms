/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zetahealth.phms.util;

import com.zetahealth.phms.data.manager.ExceptionThrowerManagerLocal;
import com.zetahealth.phms.util.exception.GeneralAppException;
import io.jsonwebtoken.Claims;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

/**
 *
 * @author buls
 */
@Stateless
public class Verifier {

    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;
       
    private String resourceUrl;   
    
    public Verifier setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
        return this;
    }
    
    public void verifyParams(String... params) throws GeneralAppException {
        for (String param : params) {
            if (param == null || param.isEmpty()) {
                exceptionManager.throwNullPharmacyAttributesException(resourceUrl);
            }
        }
    }        
    
    public Claims verifyJwt(String rawToken) 
            throws GeneralAppException {
        try {
            String authToken = rawToken.substring(7);
            Jwt  token = new Jwt();  
            return token.parseJWT(authToken);
        }  catch (Exception e) {
            exceptionManager.throwInvalidTokenException(resourceUrl);
        }
        return null;
    }
    
       
    public Claims verifyResetJwt(String resetToken) 
            throws GeneralAppException {
        try {
            Jwt token = new Jwt();  
            return token.parseResetJWT(resetToken);
        }  catch (Exception e) {
            exceptionManager.throwInvalidTokenException(resourceUrl);
        }
        return null;
    }
    
}
