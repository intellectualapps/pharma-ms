/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zetahealth.phms.util;

import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */
@Stateless
public class SecretKey {
    private String secret = "zetahealth";
    private String resetSecret = "zetahealth-reset-password";
    
    public String getSecret(){
        return secret;
    }

    public String getResetSecret() {
        return resetSecret;
    }
    
    
}
