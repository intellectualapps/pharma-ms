/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zetahealth.phms.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Lateefah
 */
public class AppPharmacy implements Serializable {
    
    private String pcnId;  
    private String name;
    private String bvn;    
    private String tin; 

    public AppPharmacy () {}

    public String getPcnId() {
        return pcnId;
    }

    public void setPcnId(String pcnId) {
        this.pcnId = pcnId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBvn() {
        return bvn;
    }

    public void setBvn(String bvn) {
        this.bvn = bvn;
    }

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }
    
    
    
}
