/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zetahealth.phms.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Lateefah
 */

@Entity
@Table(name = "pharmacy")

public class Pharmacy implements Serializable {
    
    @Id
    @NotNull
    @Column(name = "pcn_id")
    private String pcnId;
        
    @Column(name = "name")
    private String name;
        
    @NotNull
    @Column(name = "bvn")
    private String bvn;
       
    @NotNull
    @Column(name = "tin")
    private String tin;
    
    public Pharmacy () { }

    public String getPcnId() {
        return pcnId;
    }

    public void setPcnId(String pcnId) {
        this.pcnId = pcnId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBvn() {
        return bvn;
    }

    public void setBvn(String bvn) {
        this.bvn = bvn;
    }

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }
        
}
