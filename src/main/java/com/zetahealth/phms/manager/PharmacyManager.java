/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zetahealth.phms.manager;

import com.zetahealth.phms.data.manager.ExceptionThrowerManagerLocal;
import com.zetahealth.phms.data.manager.PharmacyDataManagerLocal;
import com.zetahealth.phms.model.Pharmacy;
import com.zetahealth.phms.pojo.AppBoolean;
import com.zetahealth.phms.pojo.AppPharmacy;
import com.zetahealth.phms.util.Verifier;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.zetahealth.phms.util.exception.GeneralAppException;

/**
 *
 * @author Lateefah
 */
@Stateless
public class PharmacyManager implements PharmacyManagerLocal {

    @EJB
    private PharmacyDataManagerLocal pharmacyDataManager;
    
    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;

    
    @EJB
    private Verifier verifier;

    private final String PHARMACY_LINK = "/pharmacy";

          
    @Override
    public AppPharmacy register(String pcnId, String name, String bvn, String tin) throws GeneralAppException {
        
        verifier.setResourceUrl(PHARMACY_LINK).verifyParams(pcnId, name, bvn, tin);        
                               
        
        if (pharmacyDataManager.get(pcnId) != null) {
            exceptionManager.throwPharmacyAlreadyExistException(PHARMACY_LINK);
        }

        Pharmacy pharmacy = new Pharmacy();

        pharmacy.setPcnId(pcnId);
        pharmacy.setName(name);
        pharmacy.setBvn(bvn);
        pharmacy.setTin(tin);
        
        pharmacyDataManager.create(pharmacy);
        
        return getAppPharmacy(pharmacy);
    }
    
   
    private AppPharmacy getAppPharmacy (Pharmacy pharmacy) {
        AppPharmacy appPharmacy = new AppPharmacy();
        
        appPharmacy.setPcnId(pharmacy.getPcnId());
        appPharmacy.setName(pharmacy.getName());
        appPharmacy.setBvn(pharmacy.getBvn());
        appPharmacy.setTin(pharmacy.getTin());
        
        return appPharmacy;
    }
    
    private AppBoolean getAppBoolean(Boolean status) {
        AppBoolean appBoolean = new AppBoolean();
        
        appBoolean.setStatus(status);
        
        return appBoolean;
    }

    
}
