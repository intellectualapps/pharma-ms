/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zetahealth.phms.manager;

import com.zetahealth.phms.pojo.AppPharmacy;
import com.zetahealth.phms.util.exception.GeneralAppException;

/**
 *
 * @author Lateefah
 */
public interface PharmacyManagerLocal {
    
    AppPharmacy register(String pcnId, String name, String bvn, String tin) throws GeneralAppException;
    
}