/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zetahealth.phms.service;

import com.zetahealth.phms.manager.PharmacyManagerLocal;
import com.zetahealth.phms.pojo.AppPharmacy;
import com.zetahealth.phms.util.exception.GeneralAppException;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Lateefah
 */
@Stateless
@Path("/v1/pharmacy")
public class PharmacyService {
    
    @Context
    HttpServletRequest request;   
    
    @EJB    
    PharmacyManagerLocal pharmacyManager;
       
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response register(@QueryParam("pcn-id") String pcnId,
                            @QueryParam("name") String name,
                            @QueryParam("bvn") String bvn,
                            @QueryParam("tin") String tin) throws GeneralAppException {  
        
        AppPharmacy appPharmacy = pharmacyManager.register(pcnId, name, bvn, tin);
        return Response.ok(appPharmacy).build();
           
    }      
    
}
