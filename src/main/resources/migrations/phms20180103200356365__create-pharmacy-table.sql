create table pcn_pharmacy (
    pcn_id varchar(50) not null,
    `name` varchar(255) not null,
    address varchar(2000) not null,
    constraint pk_pharmacyLookup_pcnId primary key (pcn_id)
) engine = innodb default charset = utf8;

insert into pcn_pharmacy (pcn_id, `name`, address)
    values ('4589ASE', 'Incredible Pharmacy', 'Lugbe, Abuja');
insert into pcn_pharmacy (pcn_id, `name`, address)
    values ('1289ASE', 'Medplus Pharmacy', 'Gwarinpa, Abuja');
insert into pcn_pharmacy (pcn_id, `name`, address)
    values ('4590ASE', 'New Idea Pharmacy', 'Victoria Island, Lagos');
insert into pcn_pharmacy (pcn_id, `name`, address)
    values ('4000ASE', 'Health Plus Pharmacy', 'Ikeja, Abuja');
insert into pcn_pharmacy (pcn_id, `name`, address)
    values ('8900ASE', 'Boring Pharmacy', 'Kuje, Abuja');


create table pharmacy (
    pcn_id varchar(50) not null,
    `name` varchar(255) not null,
    bvn char(10) not null,
    tin varchar(50) not null,
    constraint pk_pharmacy_pcnId primary key (pcn_id),
    constraint fk_pharmacy_pcnId foreign key (pcn_id) references pcn_pharmacy(pcn_id)
) engine = innodb default charset = utf8;